package com.example.visitors.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EnvFileServiceTest extends AbstractServiceTest<EnvFileService> {

    @Test
    void getEnvFileResult() {
        assertEquals("envFileResult",createService().getEnvFileResult());
    }

    @Override
    protected EnvFileService createService() {
        return new EnvFileService("envFileResult");
    }
}