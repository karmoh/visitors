package com.example.visitors.service;

import com.example.visitors.dto.TimeRange;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VisitorServiceTest extends AbstractServiceTest<VisitorService> {

    @Test
    void getLongestTimeRange() {
        TimeRange result = (createService().getLongestTimeRange(Arrays.asList(
                LocalTime.of(8, 15),
                LocalTime.of(8, 16),
                LocalTime.of(8, 17),
                LocalTime.of(8, 18),
                LocalTime.of(8, 19),
                LocalTime.of(8, 21),
                LocalTime.of(8, 22),
                LocalTime.of(8, 23),
                LocalTime.of(8, 24),
                LocalTime.of(8, 25),
                LocalTime.of(8, 26),
                LocalTime.of(8, 27),
                LocalTime.of(9, 27))));
        assertEquals(LocalTime.of(8, 22), result.getStartTime());
        assertEquals(LocalTime.of(8, 27), result.getEndTime());

        result = (createService().getLongestTimeRange(Collections.singletonList(
                LocalTime.of(8, 15))));
        assertEquals(LocalTime.of(8, 15), result.getStartTime());
        assertEquals(LocalTime.of(8, 15), result.getEndTime());
    }

    @Test
    void getVisitorPeakTimeAndCount() {
        String initialString = "13:00,13:05";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
        assertEquals("13:00-13:05;1", createService().getVisitorPeakTimeAndCount(targetStream));
    }

    @Override
    protected VisitorService createService() {
        return new VisitorService();
    }
}