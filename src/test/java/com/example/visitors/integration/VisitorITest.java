package com.example.visitors.integration;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.hamcrest.Matchers.equalTo;

class VisitorITest extends IntegrationTestBase {
    private static final String SERVICE_PATH = "visitors";


    @Test
    void productFlow() {
        String initialString = "13:00,13:05";
        InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
        RestAssuredMockMvc
                .given()
                .contentType(MediaType.MULTIPART_FORM_DATA_VALUE)
                .multiPart("file", "IntegrationTestFile", targetStream)
                .when()
                .post(SERVICE_PATH)
                .then()
                .log().ifError()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo("13:00-13:05;1"));

    }

    @Test
    void getEnvFileResult(){
        RestAssuredMockMvc
                .given()
                .when()
                .get(SERVICE_PATH)
                .then()
                .log().all()
                .statusCode(HttpStatus.OK.value())
                .body("data", equalTo("FILE_NOT_READ_IN"));
    }


}