package com.example.visitors.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalTime;

@Data
@Builder
public class TimeRange {

    private LocalTime startTime;
    private LocalTime endTime;
}
