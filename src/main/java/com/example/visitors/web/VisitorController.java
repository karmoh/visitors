package com.example.visitors.web;

import com.example.visitors.config.error.exception.TechnicalException;
import com.example.visitors.dto.ServiceResponse;
import com.example.visitors.service.EnvFileService;
import com.example.visitors.service.VisitorService;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@RestController
@AllArgsConstructor
public class VisitorController {

    private final VisitorService visitorService;
    private final EnvFileService envFileService;

    @ApiOperation(value = "Upload visitor times to get peak time and visitor count at the time frame," +
            "creates {filename}-result.txt result file")
    @PostMapping("/visitors")
    public ServiceResponse<String> getVisitorPeakTimeAndCount(@RequestParam("file") MultipartFile file) {
        try {
            return ServiceResponse.ok(visitorService.getVisitorPeakTimeAndCount(file.getInputStream()));
        } catch (IOException e) {
            throw new TechnicalException(e);
        }
    }

    @ApiOperation(value = "Get environment file result")
    @GetMapping("/visitors")
    public ServiceResponse<String> getEnvFileResult() {
        return ServiceResponse.ok(envFileService.getEnvFileResult());
    }
}
