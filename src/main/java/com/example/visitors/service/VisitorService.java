package com.example.visitors.service;

import com.example.visitors.config.error.exception.TechnicalException;
import com.example.visitors.dto.TimeRange;
import org.springframework.stereotype.Service;

import java.io.*;
import java.time.LocalTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class VisitorService {

    /**
     * @param inputStream file content inputStream
     * @return string value for {peak-time-start}-{peak-time-end};{visitor-count}
     */
    public String getVisitorPeakTimeAndCount(InputStream inputStream) {
        Map<LocalTime, Integer> timeVisitorCountMap = readFileLinesToVisitorCountMap(inputStream);

        Integer maxSameTimeVisitors = Collections.max(timeVisitorCountMap.values());

        List<LocalTime> maxVisitorTimes = getSortedPeakTimes(timeVisitorCountMap, maxSameTimeVisitors);

        TimeRange longestTimeRange = getLongestTimeRange(maxVisitorTimes);
        return longestTimeRange.getStartTime() + "-" + longestTimeRange.getEndTime() + ";" + maxSameTimeVisitors;
    }

    private Map<LocalTime, Integer> readFileLinesToVisitorCountMap(InputStream inputStream) {
        Map<LocalTime, Integer> timeVisitorCountMap = new HashMap<>();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {

                TimeRange timeRange = mapFileLineToTimeRange(line);

                LocalTime time = timeRange.getStartTime();
                while (time.compareTo(timeRange.getEndTime()) <= 0) {

                    if (timeVisitorCountMap.containsKey(time)) {
                        timeVisitorCountMap.put(time, timeVisitorCountMap.get(time) + 1);
                    } else {
                        timeVisitorCountMap.put(time, 1);
                    }

                    time = time.plusMinutes(1);
                }
            }
        } catch (IOException e) {
            throw new TechnicalException(e);
        }

        return timeVisitorCountMap;
    }

    private TimeRange mapFileLineToTimeRange(String line) {
        try {
            String[] split = line.split(",");
            return TimeRange.builder()
                    .startTime(LocalTime.parse(split[0]))
                    .endTime(LocalTime.parse(split[1]))
                    .build();
        } catch (Exception e) {
            throw new TechnicalException("Invalid input format");
        }
    }

    private List<LocalTime> getSortedPeakTimes(Map<LocalTime, Integer> timeVisitorCountMap, Integer maxSameTimeVisitors) {
        return timeVisitorCountMap.entrySet().stream()
                .filter(localTimeIntegerEntry -> localTimeIntegerEntry.getValue()
                        .equals(maxSameTimeVisitors))
                .map(Map.Entry::getKey)
                .sorted().collect(Collectors.toList());
    }

    TimeRange getLongestTimeRange(List<LocalTime> maxVisitorTimes) {
        AtomicReference<LocalTime> start = new AtomicReference<>();
        AtomicReference<LocalTime> end = new AtomicReference<>();

        AtomicReference<Integer> longestTimeCounter = new AtomicReference<>(0);
        AtomicReference<TimeRange> longestTimeHolder = new AtomicReference<>();
        AtomicReference<Integer> periodLengthCounter = new AtomicReference<>(1);
        AtomicReference<LocalTime> prevTime = new AtomicReference<>();

        maxVisitorTimes.forEach(localTime -> {
            if (start.get() == null) {
                start.set(localTime);
            }
            if (end.get() == null || end.get().compareTo(localTime) < 0) {
                end.set(localTime);
            }
            if (prevTime.get() != null && prevTime.get().plusMinutes(1).compareTo(localTime) != 0) {
                prevTime.set(null);
                periodLengthCounter.set(1);
                start.set(null);
                end.set(null);
                return;
            }
            if (longestTimeCounter.get() < periodLengthCounter.get()) {
                longestTimeCounter.set(periodLengthCounter.get());
                longestTimeHolder.set(TimeRange.builder()
                        .startTime(start.get())
                        .endTime(end.get())
                        .build());
            }
            periodLengthCounter.set(periodLengthCounter.get() + 1);
            prevTime.set(localTime);

        });
        return longestTimeHolder.get();
    }

}
