package com.example.visitors.service;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class EnvFileService {


    private final String envFileResult;

    public EnvFileService(@Qualifier("envFileResult") String envFileResult) {
        this.envFileResult = envFileResult;
    }

    public String getEnvFileResult() {
        return envFileResult;
    }

}
