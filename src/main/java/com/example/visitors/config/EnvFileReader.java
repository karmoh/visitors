package com.example.visitors.config;


import com.example.visitors.config.error.exception.TechnicalException;
import com.example.visitors.service.VisitorService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;


@Log4j2
@Configuration
@AllArgsConstructor
public class EnvFileReader {

    private final VisitorService visitorService;

    @Bean("envFileResult")
    public String readEnvironmentFile() {
        String filePath = System.getenv("FILE_PATH");
        if (StringUtils.isEmpty(filePath)) {
            return "FILE_NOT_READ_IN";
        }
        try {
            File file = new File(filePath);
            String result = visitorService.getVisitorPeakTimeAndCount(new FileInputStream(file));
            log.info("Environment file result: {}", result);
            return result;
        } catch (Exception e) {
            throw new TechnicalException(e);
        }
    }
}
