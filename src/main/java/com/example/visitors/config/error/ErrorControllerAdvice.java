package com.example.visitors.config.error;

import com.example.visitors.config.error.exception.TechnicalException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.UUID;

@Log4j2
@ControllerAdvice
@RequestMapping(produces = "application/json")
public class ErrorControllerAdvice {


    @ExceptionHandler({TechnicalException.class, Exception.class})
    public ResponseEntity<ErrorMessage> technicalException(final Exception exception) {
        String uuid = UUID.randomUUID().toString();
        ErrorMessage responseMessage = new ErrorMessage(uuid, ErrorCodes.TECHNICAL_EXCEPTION.getErrorCode(), LocalDateTime.now(Clock.systemUTC()), null);
        log.error(responseMessage.toString(), exception);
        return new ResponseEntity<>(responseMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
