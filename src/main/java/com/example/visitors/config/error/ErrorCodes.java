package com.example.visitors.config.error;

public enum ErrorCodes {
    TECHNICAL_EXCEPTION;


    public String getErrorCode() {
        return this.name().toLowerCase();
    }
}
