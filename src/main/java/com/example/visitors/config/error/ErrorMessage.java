package com.example.visitors.config.error;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ErrorMessage {
  private String ref;
  private String errorCode;
  private LocalDateTime dateTime;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Object[] arguments;

}
