## Ajavahemiku leidmine

* Fail loetakse sisse rida haaval.
* Igal real käiakse üle kõik külastaja oldud minutid ja lisatakse need kogu faili minuti ja külastaja map-i.
* Map-ist otsitakse välja maksimaalne külastajate arv minuti kohta.
* Maksimaalset külastajate arvu minuti kohta kasutatakse, et leida kõik minutid kus on olnud vastavalt inimesi, tulem sorteeritakse.
* Kõik minutid käiakse üle, et leida kõige pikem maksimaalsete külstajatega aja vahemik.

