# Visitors

## Running application

optional environment parameter for visitor file : `FILE_PATH`
```
gradlew bootRun
```

Go to [Swagger](http://localhost:8080/swagger-ui.html) resources

### Prerequisites

You will need Java 14

## Running the tests

```
gradlew test
```

## Built With

* Java14
* Spring Boot
* Lombok
* SpringFox
* REST Assured